//
//  ViewController.swift
//  CurrencyRate
//
//  Created by PC on 23/01/19.
//  Copyright © 2019 reveralto. All rights reserved.
//

import UIKit
import PopupDialog
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ViewController: UIViewController, UIAdaptivePresentationControllerDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectCurrencyButton: UIButton!
    @IBOutlet weak var currencyContainerView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var mainView: UIView!
    
    let loaderSize = CGSize(width: 50, height: 50)
    var currencyList: [String] = ["USD","EUR","AUD","BHD","QAR","INR"]
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        
        //Check Name is stored or not
        self.checkForName()
        currencyContainerView.setBorder(width: 1.5, color: .white)
        
        GlobalNotification.addObserver(self, selector: #selector(hideLoaderAnimation), name: NSNotification.Name(rawValue: notification_hide_loader), object: nil)
    }
    
    func checkForName()
    {
        if let name = GlobalUserDefault.object(forKey: "Name") as? String {
            self.nameLabel.text = "Hey, \(name)"
            nameLabel.isHidden = false
            currencyContainerView.isHidden = false
            mainView.isHidden = false
        } else{
            nameLabel.isHidden = true
            currencyContainerView.isHidden = true
            mainView.isHidden = true
            self.perform(#selector(presentNameViewPopUp), with: nil, afterDelay: 0.5)
        }
    }
    
    //MARK:- present Custom View
    @objc func presentNameViewPopUp() {
        
        let nameAlert = self.storyboard?.instantiateViewController(withIdentifier: "NameAlertViewController") as! NameAlertViewController
        
        let popup = PopupDialog(viewController: nameAlert, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false, panGestureDismissal: false)
        
        let overlayAppearance = PopupDialogOverlayView.appearance()
        overlayAppearance.color           = .clear
        overlayAppearance.blurRadius      = 12
        overlayAppearance.blurEnabled     = true
        overlayAppearance.liveBlurEnabled = false
        overlayAppearance.opacity         = 0.7
        
        nameAlert.onCompletion = {(name) -> Void in
            print(name)
            self.nameLabel.text = "Hey, \(name)"
            self.nameLabel.isHidden = false
            self.currencyContainerView.isHidden = false
            self.mainView.isHidden = false
            popup.dismiss()
        }
        self.present(popup, animated: true, completion: nil)
    }
    
    //MARK:- Set Currency List
    func SetCurrencyList() {
        DPPickerManager.shared.showPicker(title: "Select Currency", selected: currencyList.first, strings: currencyList) { (value, index, cancel) in
            if !cancel {
                toValue = self.currencyList[index]
                self.selectCurrencyButton.setTitle(self.currencyList[index].uppercased(), for: .normal)
                self.mainView.isHidden = false
            }
        }
    }
    
    //MARK:-  delegate UIPresentationController
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    //MARK:-  API Call
    func getPriceData() {
        
        self.showLoaderAnimation()
        getAPICallToFetchCurrencyPrice(url: currentCurrencyPrice()) { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                self.hideLoaderAnimation()
                print("json: ", json.dictionary ?? "")
                if let _ = json.dictionary {
                    do {
                        let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String: Any]
                        print("someDictionaryFromJSON : ", (someDictionaryFromJSON["price"] as? Double) ?? 0.0)
                        
                        let vcPrice = self.storyboard?.instantiateViewController(withIdentifier: "PriceViewController") as! PriceViewController
                        vcPrice.price = "\((someDictionaryFromJSON["price"] as? Double) ?? 0.0)"
                        self.navigationController?.pushViewController(vcPrice, animated: true)
                    } catch {
                        print("error: \(error)")
                    }
                }
            case .failure(let error):
                self.hideLoaderAnimation()
                print("Request failed with error: \(error.localizedDescription)")
            }
        }
    }
    
    //MARK:- Show Loader
    func showLoaderAnimation() {
        self.startAnimating(loaderSize, message: "Loading...", messageFont: UIFont(name:"Poppins Medium", size: 18.0), type: NVActivityIndicatorType.lineScale, color: UIColor.white, padding: 5, displayTimeThreshold: 0, minimumDisplayTime: 0, backgroundColor: UIColor.clear, textColor: UIColor.white, fadeInAnimation: nil)
        self.blurView.isHidden = false
    }
    
    //MARK:- Hide Loader
   @objc func hideLoaderAnimation() {
        DispatchQueue.main.async {
            self.stopAnimating()
            self.blurView.isHidden = true
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func presentCurrencyButtonAction(_ sender: UIButton) {
        SetCurrencyList()
    }
    
    @IBAction func getCurrencyRateButtonAction(_ sender: UIButton) {
        if toValue != nil {
            getPriceData()
        }
        else {
            showAlert("Error", error: "Please select any currency!!")
        }
    }
    
    //MARK:- Dealloc
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

