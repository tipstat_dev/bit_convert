//
//  PriceViewController.swift
//  CurrencyRate
//
//  Created by PC on 23/01/19.
//  Copyright © 2019 reveralto. All rights reserved.
//

import UIKit

class PriceViewController: UIViewController {

     var price: String?
    
    @IBOutlet weak var firstCurrencyRate: UILabel!
    @IBOutlet weak var secondCurrencyRate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        firstCurrencyRate.text = "1  \(fromValue)".uppercased()
        secondCurrencyRate.text = "\(toValue ?? "") \(price ?? "0")".uppercased()
        GlobalNotification.addObserver(self, selector: #selector(backButtonAction(_:)), name: NSNotification.Name(rawValue: notification_back), object: nil)
    }
    
    //MARK:- Button Action
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Dealloc
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
