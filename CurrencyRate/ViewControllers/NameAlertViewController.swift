//
//  NameAlertViewController.swift
//  CurrencyRate
//
//  Created by PC on 23/01/19.
//  Copyright © 2019 reveralto. All rights reserved.
//

import UIKit

class NameAlertViewController: UIViewController {
 
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    
    typealias CompletionBlock = (_ name: String) -> Void
    var onCompletion:CompletionBlock?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        doneButton.layer.cornerRadius = 4
    }

    //MARK:- Button Action
    @IBAction func doneButtonAction(_ sender: UIButton) {
        if nameTextField.text == "" || nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 == 0 {
            return
        } else{
            GlobalUserDefault.set(nameTextField.text, forKey: "Name")
            if let isCompletion = self.onCompletion {
                isCompletion(nameTextField.text ?? "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
