//
//  Global.swift
//  CurrencyRate
//
//  Created by PC on 23/01/19.
//  Copyright © 2019 reveralto. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

let myApp = UIApplication.shared.delegate as! AppDelegate
let GlobalUserDefault = UserDefaults.standard
let GlobalNotification = NotificationCenter.default

var fromValue = "BTC"
var toValue: String?

var url_currency_price = "https://apiv2.bitcoinaverage.com/convert/global?from=\(fromValue)&to=\(toValue ?? "")"

var popUpWidth = UIScreen.main.bounds.width - 100
var popUpHeight = 200.0 as CGFloat

//MARK:- Notification Name

let notification_back = "notification_back"
let notification_hide_loader = "notification_hide_loader"

func currentCurrencyPrice() -> String {
    url_currency_price = "https://apiv2.bitcoinaverage.com/convert/global?from=\(fromValue)&to=\(toValue ?? "")"
    return url_currency_price
}

//MARK:- Check Internet connection
func isInternetConnected() -> Bool {
    let reachability: Reachability? = Reachability.init()
    print(reachability!.isReachable)
    return reachability!.isReachable
}

//MARK:- Price API call
func getAPICallToFetchCurrencyPrice(url: String, completion: @escaping (DataResponse<Any>) -> ()) {
    
    if isInternetConnected() {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
            completion(response)
        }
    }
    else {
        GlobalNotification.post(name: NSNotification.Name(rawValue: notification_hide_loader), object: nil, userInfo: nil)
        showAlert("Error", error: "No internet Connection!!")
    }
}

//MARK:- Show Error Alert

func showAlert(_ title:String, error:String) {
    let alertController = UIAlertController(title: title, message: error, preferredStyle: .alert)
    
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(action)
    
    myApp.window?.rootViewController?.present(alertController, animated: true, completion: nil)
}

extension UIView {
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            if newValue <= 0 {
                return
            }
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    public func setBorder(width:CGFloat = 1, color: UIColor = UIColor.darkGray)
    {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    public func setCornerRadious(radious:CGFloat = 4)
    {
        self.layer.cornerRadius = radious
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    public func setRoundCornerRadious()
    {
        //        let sw = UIScreen.main.bounds.size.width
        self.layer.cornerRadius = CGFloat(self.frame.width/2.0)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 2.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        self.layer.masksToBounds = false
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 3) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
}
